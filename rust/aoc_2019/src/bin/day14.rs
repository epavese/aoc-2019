use std::collections::HashMap;
use std::collections::HashSet;
use std::env;
use std::fs;

#[derive(Debug)]
pub struct Reaction {
    pub synth: (String, u16),
    pub reagents: HashSet<(String, u16)>,
}

fn make_product_quantity(s: &str) -> (String, u16) {
    let mut it = s.split(" ");
    let qty = it.next().unwrap().parse::<u16>().unwrap();
    let name  = it.next().unwrap().to_owned();
    (name, qty)
}

fn make_reagents_quantities(s: &str) -> HashSet<(String, u16)> {
    s.split(", ").fold(HashSet::new(), |mut s, elem_str| {s.insert(make_product_quantity(elem_str)); s})
}

fn make_reaction(s: &str) -> Reaction {
    let mut it = s.split(" => ");
    let reagents_str = it.next().unwrap();
    let synth_str = it.next().unwrap();

    Reaction{ synth: make_product_quantity(synth_str), reagents: make_reagents_quantities(reagents_str)}
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let input_file = args
        .get(1)
        .expect("You need to provide the input file as argument");

    let reactions: HashMap<String,(u16,HashSet<(String,u16)>)> = fs::read_to_string(input_file)
        .expect("Error reading file")
        .trim()
        .split('\n')
        .map(make_reaction)
        .fold(HashMap::new(), |mut m, r| {m.insert(r.synth.0, (r.synth.1,r.reagents)); m});

    println!("{:?}", reactions);
}
