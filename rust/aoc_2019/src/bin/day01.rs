use std::env;
use std::fs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let input_file = args
        .get(1)
        .expect("You need to provide the input file as argument");
    let masses: Vec<u64> = fs::read_to_string(input_file)
        .expect("Error reading input file")
        .split_whitespace()
        .map(|s| s.parse::<u64>().expect("Error parsing u64 from file"))
        .collect();

    println!(
        "Day 01, part 1: total fuel needed is {}",
        masses
            .iter()
            .map(|m| get_fuel_for_mass(*m, false))
            .sum::<u64>()
    );

    println!(
        "Day 01, part 2: total fuel needed is {}",
        masses
            .iter()
            .map(|m| get_fuel_for_mass(*m, true))
            .sum::<u64>()
    );
}

fn get_fuel_for_mass(mass: u64, fixpoint: bool) -> u64 {
    let fuel = (mass / 3).saturating_sub(2);

    if fixpoint && fuel > 0 {
        fuel + get_fuel_for_mass(fuel, fixpoint)
    } else {
        fuel
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn part_1_tests() {
        assert_eq!(2, super::get_fuel_for_mass(12, false));
        assert_eq!(2, super::get_fuel_for_mass(14, false));
        assert_eq!(654, super::get_fuel_for_mass(1969, false));
        assert_eq!(33583, super::get_fuel_for_mass(100756, false));
    }

    #[test]
    fn part_2_tests() {
        assert_eq!(2, super::get_fuel_for_mass(14, true));
        assert_eq!(966, super::get_fuel_for_mass(1969, true));
        assert_eq!(50346, super::get_fuel_for_mass(100756, true));
    }
}
