use aoc_2019::intcode::*;
use std::env;
use std::fs;

const PHASES: u64 = 2310; // 2*3*5*7*11 = (0,1,2,3,4)\
const CODED_PHASES: [u64; 5] = [2, 3, 5, 7, 11];

#[derive(Debug)]
struct AmpConfig {
    phases: Vec<u64>,
    available: u64,
}

fn main() -> Result<(), IntCodeInterpreterError> {
    let args: Vec<String> = env::args().collect();
    let input_file = args
        .get(1)
        .expect("You need to provide the input file as argument");
    let mem_space: Vec<i64> = fs::read_to_string(input_file)
        .expect("Error reading file")
        .trim()
        .split(',')
        .map(|s| s.parse::<i64>().expect("Error reading u64 from file"))
        .collect();

    let mut max_thruster_signal = 0;
    let mut max_thruster_signal_loopback = 0;

    let init = AmpConfig {
        phases: vec![],
        available: PHASES,
    };

    let mut work_stack = Vec::new();
    work_stack.push(init);
    while !work_stack.is_empty() {
        let next = work_stack.pop().unwrap();
        if next.available == 1 {
            let signal = try_phases_configuration(&next, &mem_space);
            if signal > max_thruster_signal {
                max_thruster_signal = signal;
            }

            let next_loopback = AmpConfig {
                phases: next.phases.iter().map(|x| x + 5).collect(),
                available: 1,
            };
            let signal = try_phases_configuration_loopback(&next_loopback, &mem_space);
            if signal > max_thruster_signal_loopback {
                max_thruster_signal_loopback = signal;
            }
        } else {
            work_stack.append(&mut next_configs(next));
        }
    }

    println!("Day 07, part 1: result is {}", max_thruster_signal);

    println!("Day 07, part 2: result is {}", max_thruster_signal_loopback);

    Ok(())
}

fn try_phases_configuration_loopback(cfg: &AmpConfig, mem_space: &[i64]) -> i64 {
    let mut running: [bool; 5] = [true, true, true, true, true];
    let mut last_output: [i64; 5] = [0, 0, 0, 0, 0];
    let mut amplifiers = vec![
        IntCodeInterpreter::new(mem_space.to_owned()),
        IntCodeInterpreter::new(mem_space.to_owned()),
        IntCodeInterpreter::new(mem_space.to_owned()),
        IntCodeInterpreter::new(mem_space.to_owned()),
        IntCodeInterpreter::new(mem_space.to_owned()),
    ];

    for (n_amp, amp) in amplifiers.iter_mut().enumerate() {
        amp.set_input(cfg.phases[n_amp] as i64);
    }

    amplifiers[0].set_input(0);

    while running.iter().any(|x| *x) {
        for i in 0..5 {
            if !running[i] {
                continue;
            }

            let run_result = amplifiers[i].run_concurrent().expect("Should be ok");
            while let Some(n) = amplifiers[i].get_output() {
                last_output[i] = n;
                amplifiers[(i + 1) % 5].set_input(n);
            }

            match run_result {
                IntCodeInterpreterStatus::Done => running[i] = false,
                _input_or_output => (),
            }
        }
    }

    last_output[4]
}

fn try_phases_configuration(cfg: &AmpConfig, mem_space: &[i64]) -> i64 {
    let mut input = 0;
    for i in 0..5 {
        let mut interpreter = IntCodeInterpreter::new(mem_space.to_owned());
        interpreter.set_input(cfg.phases[i] as i64);
        interpreter.set_input(input);
        interpreter.run_concurrent().expect("Should not panic here");
        input = interpreter.get_output().unwrap();
    }

    input
}

fn next_configs(cfg: AmpConfig) -> Vec<AmpConfig> {
    let mut res: Vec<AmpConfig> = Vec::new();
    let available = cfg.available;
    for phase in CODED_PHASES.iter() {
        if available % phase == 0 {
            let mut new_phases = cfg.phases.clone();
            new_phases.push(*phase as u64);
            res.push(AmpConfig {
                phases: new_phases,
                available: available / phase,
            });
        }
    }
    res
}

#[cfg(test)]
mod tests_amplifiers {
    use super::*;

    #[test]
    fn test_max_signal_1() {
        let mem_space = vec![
            3, 15, 3, 16, 1002, 16, 10, 16, 1, 16, 15, 15, 4, 15, 99, 0, 0,
        ];
        let cfg = AmpConfig {
            phases: vec![4, 3, 2, 1, 0],
            available: 1,
        };
        let result = try_phases_configuration(&cfg, &mem_space);
        assert_eq!(43210, result);
    }

    #[test]
    fn test_max_signal_2() {
        let mem_space = vec![
            3, 23, 3, 24, 1002, 24, 10, 24, 1002, 23, -1, 23, 101, 5, 23, 23, 1, 24, 23, 23, 4, 23,
            99, 0, 0,
        ];
        let cfg = AmpConfig {
            phases: vec![0, 1, 2, 3, 4],
            available: 1,
        };
        let result = try_phases_configuration(&cfg, &mem_space);
        assert_eq!(54321, result);
    }

    #[test]
    fn test_max_signal_3() {
        let mem_space = vec![
            3, 31, 3, 32, 1002, 32, 10, 32, 1001, 31, -2, 31, 1007, 31, 0, 33, 1002, 33, 7, 33, 1,
            33, 31, 31, 1, 32, 31, 31, 4, 31, 99, 0, 0, 0,
        ];
        let cfg = AmpConfig {
            phases: vec![1, 0, 4, 3, 2],
            available: 1,
        };
        let result = try_phases_configuration(&cfg, &mem_space);
        assert_eq!(65210, result);
    }

    #[test]
    fn test_max_signal_loopback_1() {
        let mem_space = vec![
            3, 26, 1001, 26, -4, 26, 3, 27, 1002, 27, 2, 27, 1, 27, 26, 27, 4, 27, 1001, 28, -1,
            28, 1005, 28, 6, 99, 0, 0, 5,
        ];
        let cfg = AmpConfig {
            phases: vec![9, 8, 7, 6, 5],
            available: 1,
        };
        let result = try_phases_configuration_loopback(&cfg, &mem_space);
        assert_eq!(139629729, result);
    }

    #[test]
    fn test_max_signal_loopback_2() {
        let mem_space = vec![
            3, 52, 1001, 52, -5, 52, 3, 53, 1, 52, 56, 54, 1007, 54, 5, 55, 1005, 55, 26, 1001, 54,
            -5, 54, 1105, 1, 12, 1, 53, 54, 53, 1008, 54, 0, 55, 1001, 55, 1, 55, 2, 53, 55, 53, 4,
            53, 1001, 56, -1, 56, 1005, 56, 6, 99, 0, 0, 0, 0, 10,
        ];
        let cfg = AmpConfig {
            phases: vec![9, 7, 8, 5, 6],
            available: 1,
        };
        let result = try_phases_configuration_loopback(&cfg, &mem_space);
        assert_eq!(18216, result);
    }
}
