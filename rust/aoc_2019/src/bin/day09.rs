use aoc_2019::intcode::*;
use std::env;
use std::fs;

fn main() -> Result<(), IntCodeInterpreterError> {
    let args: Vec<String> = env::args().collect();
    let input_file = args
        .get(1)
        .expect("You need to provide the input file as argument");
    let mem_space: Vec<i64> = fs::read_to_string(input_file)
        .expect("Error reading file")
        .trim()
        .split(',')
        .map(|s| s.parse::<i64>().expect("Error reading u64 from file"))
        .collect();

    let mut interpreter = IntCodeInterpreter::new(mem_space.clone());
    interpreter.set_input(1);
    interpreter.run()?;
    println!(
        "Day 09, part 1: result is {}",
        interpreter.get_output().expect("Should not panic here")
    );

    let mut interpreter = IntCodeInterpreter::new(mem_space);
    interpreter.set_input(2);
    interpreter.run()?;
    println!(
        "Day 09, part 2: result is {}",
        interpreter.get_output().expect("Should not panic here")
    );

    Ok(())
}

#[cfg(test)]
mod tests_relative {
    use super::*;

    #[test]
    fn test_program_relative_1() {
        let mem_space: Vec<i64> = vec![
            109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99,
        ];
        let mut interpreter = IntCodeInterpreter::new(mem_space.clone());
        interpreter.run().expect("Should not panic here");

        let mut res = Vec::new();
        while let Some(n) = interpreter.get_output() {
            res.push(n);
        }

        assert_eq!(mem_space, res);
    }

    #[test]
    fn test_program_relative_2() {
        let mem_space: Vec<i64> = vec![1102, 34915192, 34915192, 7, 4, 7, 99, 0];
        let mut interpreter = IntCodeInterpreter::new(mem_space.clone());
        interpreter.run().expect("Should not panic here");

        let output = interpreter.get_output().unwrap();
        assert!(999999999999999 < output);
        assert!(10000000000000000 > output);
    }

    #[test]
    fn test_program_relative_3() {
        let mem_space: Vec<i64> = vec![104, 1125899906842624, 99];
        let mut interpreter = IntCodeInterpreter::new(mem_space.clone());
        interpreter.run().expect("Should not panic here");

        let output = interpreter.get_output().unwrap();
        assert_eq!(1125899906842624, output);
    }
}
