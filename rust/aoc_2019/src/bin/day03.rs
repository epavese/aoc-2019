use std::cmp;
use std::collections::HashSet;
use std::env;
use std::fs;

// Point
#[derive(Eq, PartialEq, Hash, Debug, Clone)]
struct Point {
    x: i32,
    y: i32,
}
// Point

// Segment
#[derive(Debug, Clone, Hash, PartialEq)]
struct Segment {
    from: Point,
    to: Point,
    len: u32,
}

impl Eq for Segment {}

impl Segment {
    fn new(from: Point, to: Point) -> Segment {
        let len = distance(&from, &to);
        if from.x < to.x || (from.x == to.x && from.y > to.y) {
            Segment { from, to, len }
        } else {
            Segment {
                from: to,
                to: from,
                len,
            }
        }
    }

    fn is_vertical(&self) -> bool {
        self.from.x == self.to.x
    }

    fn is_horizontal(&self) -> bool {
        !self.is_vertical()
    }

    fn len(&self) -> u32 {
        self.len
    }

    fn is_parallel_to(&self, other: &Self) -> bool {
        self.is_vertical() == other.is_vertical()
    }

    fn intersection(&self, other: &Self) -> Option<Segment> {
        if self.is_parallel_to(other) {
            self.get_multi_intersection(other)
        } else {
            self.get_point_intersection(other)
        }
    }

    // TODO rewrite
    fn get_multi_intersection(&self, other: &Self) -> Option<Segment> {
        if !self.is_parallel_to(other) {
            None
        } else if self.is_vertical() {
            if self.from.x == other.from.x {
                let self_start = cmp::min(self.from.y, self.to.y);
                let self_end = cmp::max(self.from.y, self.to.y);
                let other_start = cmp::min(other.from.y, other.to.y);
                let other_end = cmp::max(other.from.y, other.to.y);
                let int_start = cmp::max(self_start, other_start);
                let int_end = cmp::min(self_end, other_end);
                if int_start <= int_end {
                    Some(Segment::new(
                        Point {
                            x: self.from.x,
                            y: int_start,
                        },
                        Point {
                            x: self.from.x,
                            y: int_end,
                        },
                    ))
                } else {
                    None
                }
            } else {
                None
            }
        } else if self.from.y == other.from.y {
            let self_start = cmp::min(self.from.x, self.to.x);
            let self_end = cmp::max(self.from.x, self.to.x);
            let other_start = cmp::min(other.from.x, other.to.x);
            let other_end = cmp::max(other.from.x, other.to.x);
            let int_start = cmp::max(self_start, other_start);
            let int_end = cmp::min(self_end, other_end);
            if int_start <= int_end {
                Some(Segment::new(
                    Point {
                        x: int_start,
                        y: self.from.y,
                    },
                    Point {
                        x: int_end,
                        y: self.from.y,
                    },
                ))
            } else {
                None
            }
        } else {
            None
        }
    }

    fn get_point_intersection(&self, other: &Self) -> Option<Segment> {
        if self.is_parallel_to(other) {
            None
        } else if self.is_vertical() {
            let top = cmp::max(self.from.y, self.to.y);
            let bot = cmp::min(self.from.y, self.to.y);
            let left = cmp::min(other.from.x, other.to.x);
            let right = cmp::max(other.from.x, other.to.x);
            if left <= self.from.x
                && right >= self.from.x
                && bot <= other.from.y
                && top >= other.from.y
            {
                let p = Point {
                    x: self.from.x,
                    y: other.from.y,
                };
                Some(Segment::new(p.clone(), p))
            } else {
                None
            }
        } else {
            other.get_point_intersection(self)
        }
    }

    fn contains(&self, pt: &Point) -> bool {
        self.intersection(&Segment::new(pt.clone(), pt.clone()))
            .is_some()
    }
}

impl std::iter::IntoIterator for Segment {
    type Item = Point;
    type IntoIter = SegmentIterator;

    fn into_iter(self) -> Self::IntoIter {
        SegmentIterator {
            index: 0,
            is_vertical: self.is_vertical(),
            len: self.len(),
            start: {
                if (self.is_vertical() && self.from.y < self.to.y)
                    || (self.is_horizontal() && self.from.x < self.to.x)
                {
                    self.from
                } else {
                    self.to
                }
            },
        }
    }
}

struct SegmentIterator {
    index: usize,
    is_vertical: bool,
    len: u32,
    start: Point,
}

impl Iterator for SegmentIterator {
    type Item = Point;
    fn next(&mut self) -> Option<Point> {
        if self.index > self.len as usize {
            None
        } else {
            self.index += 1;
            if self.is_vertical {
                Some(Point {
                    x: self.start.x,
                    y: self.start.y + (self.index as i32 - 1),
                })
            } else {
                Some(Point {
                    x: self.start.x + (self.index as i32 - 1),
                    y: self.start.y,
                })
            }
        }
    }
}
// Segment

const PORT: Point = Point { x: 0, y: 0 };

fn distance(p1: &Point, p2: &Point) -> u32 {
    ((p1.x - p2.x).abs() + (p1.y - p2.y).abs()) as u32
}

fn get_intersections(wire1: &[Segment], wire2: &[Segment]) -> HashSet<Point> {
    let mut intersections: HashSet<Segment> = HashSet::new();
    for s1 in wire1 {
        for s2 in wire2 {
            match s1.intersection(s2) {
                None => continue,
                Some(x) => {
                    intersections.insert(x);
                }
            }
        }
    }

    let intersection_points: HashSet<Point> = intersections
        .iter()
        .flat_map(|x| x.clone().into_iter())
        .collect();

    intersection_points
}

fn distance_to_closest_intersection(intersection_points: &HashSet<Point>) -> u32 {
    let mut min_dist = u32::max_value();
    for pt in intersection_points {
        let d = distance(&pt, &PORT);
        if d > 0 && d < min_dist {
            min_dist = d;
        }
    }

    min_dist
}

fn move_into_segment(from: Option<&Point>, mv: Option<&str>) -> Option<Segment> {
    let from = from?;
    let mv_step = mv?[1..]
        .parse::<i32>()
        .expect("Error parsing i32 from path");
    match mv?.chars().next().expect("Invalid move") {
        'L' => {
            let to = Point {
                x: from.x - mv_step,
                y: from.y,
            };
            Some(Segment::new(from.clone(), to))
        }
        'R' => {
            let to = Point {
                x: from.x + mv_step,
                y: from.y,
            };
            Some(Segment::new(from.clone(), to))
        }
        'U' => {
            let to = Point {
                x: from.x,
                y: from.y + mv_step,
            };
            Some(Segment::new(from.clone(), to))
        }
        'D' => {
            let to = Point {
                x: from.x,
                y: from.y - mv_step,
            };
            Some(Segment::new(from.clone(), to))
        }
        err => panic!("Unexpected move direction {}", err),
    }
}

fn path_into_segments(s: &str) -> Vec<Segment> {
    let mut moves = s.split(',');
    let first =
        move_into_segment(Some(&PORT), moves.next()).expect("Path must have at least one step");
    let first_dest = if first.from == PORT {
        first.to.clone()
    } else {
        first.from.clone()
    };
    let segs_iter = std::iter::successors(Some((first, first_dest)), |x| {
        let step = move_into_segment(Some(&x.1), moves.next())?;
        let step_dest = if step.from == x.1 {
            &step.to
        } else {
            &step.from
        };
        Some((step.clone(), step_dest.clone()))
    });
    segs_iter.map(|x| x.0).collect()
}

fn get_wire_distance_to_point(wire: &[Segment], pt: &Point) -> u32 {
    let mut dist: u32 = 0;
    let mut start = &PORT;
    for segment in wire {
        if segment.contains(&pt) {
            dist += distance(start, &pt);
            break;
        } else {
            start = if segment.from == *start {
                &segment.to
            } else {
                &segment.from
            };
            dist += segment.len();
        }
    }

    dist
}

fn get_best_intersection_in_wire_distance(
    wire1: &[Segment],
    wire2: &[Segment],
    intersections: &HashSet<Point>,
) -> u32 {
    let mut best_distance = u32::max_value();
    for pt in intersections {
        let new_distance =
            get_wire_distance_to_point(wire1, pt) + get_wire_distance_to_point(wire2, pt);
        if new_distance > 0 && new_distance < best_distance {
            best_distance = new_distance;
        }
    }

    best_distance
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let input_file = args
        .get(1)
        .expect("You need to provide the input file as argument");
    let paths: Vec<Vec<Segment>> = fs::read_to_string(input_file)
        .expect("Error reading input file")
        .split_whitespace()
        .map(|s| path_into_segments(s))
        .collect();

    let intersections = get_intersections(&paths[0], &paths[1]);
    let min_dist = distance_to_closest_intersection(&intersections);
    println!("Day 03, part 1: result is {}", min_dist);

    let best_dist = get_best_intersection_in_wire_distance(&paths[0], &paths[1], &intersections);
    println!("Day 03, part 2: result is {}", best_dist);
}

#[cfg(test)]
mod tests {
    use super::{Point, Segment};
    use std::collections::HashSet;

    #[test]
    fn test_simple_intersection() {
        let s1 = Segment::new(Point { x: 1, y: 1 }, Point { x: 1, y: -5 });
        let s2 = Segment::new(Point { x: 0, y: 0 }, Point { x: 10, y: 0 });
        let inter = Segment::new(Point { x: 1, y: 0 }, Point { x: 1, y: 0 });
        assert_eq!(inter, s1.intersection(&s2).expect("Should intersect"));
        assert_eq!(inter, s2.intersection(&s1).expect("Should intersect"));
    }

    #[test]
    fn test_simple_no_intersection() {
        let s1 = Segment::new(Point { x: 1, y: 1 }, Point { x: 1, y: -5 });
        let s2 = Segment::new(Point { x: 4, y: 0 }, Point { x: 10, y: 0 });
        assert!(s1.intersection(&s2).is_none());
        assert!(s2.intersection(&s1).is_none());
    }

    #[test]
    fn test_point_intersections_at_ends() {
        let s1 = Segment::new(Point { x: 1, y: 1 }, Point { x: 1, y: -5 });
        let s2 = Segment::new(Point { x: 1, y: -2 }, Point { x: 10, y: -2 });
        let inter = Segment::new(Point { x: 1, y: -2 }, Point { x: 1, y: -2 });
        assert_eq!(inter, s1.intersection(&s2).expect("Should intersect"));
        assert_eq!(inter, s2.intersection(&s1).expect("Should intersect"));
    }

    #[test]
    fn test_parallel_intersections_at_ends() {
        let s1 = Segment::new(Point { x: 1, y: 1 }, Point { x: 1, y: -5 });
        let s2 = Segment::new(Point { x: 1, y: -5 }, Point { x: 1, y: -8 });
        let inter = Segment::new(Point { x: 1, y: -5 }, Point { x: 1, y: -5 });
        assert_eq!(inter, s1.intersection(&s2).expect("Should intersect"));
        assert_eq!(inter, s2.intersection(&s1).expect("Should intersect"));
    }

    #[test]
    fn test_parallel_no_segment_intersection() {
        let s1 = Segment::new(Point { x: 1, y: 1 }, Point { x: 1, y: -5 });
        let s2 = Segment::new(Point { x: 1, y: -6 }, Point { x: 1, y: -8 });
        let s3 = Segment::new(Point { x: 4, y: -6 }, Point { x: 4, y: -8 });
        assert!(s1.intersection(&s2).is_none());
        assert!(s1.intersection(&s3).is_none());
        assert!(s2.intersection(&s1).is_none());
        assert!(s2.intersection(&s3).is_none());
        assert!(s3.intersection(&s1).is_none());
        assert!(s3.intersection(&s2).is_none());
    }

    #[test]
    fn test_segment_overlap() {
        let s1 = Segment::new(Point { x: 1, y: 1 }, Point { x: 1, y: -5 });
        let s2 = Segment::new(Point { x: 1, y: 8 }, Point { x: 1, y: -1 });
        let inter = Segment::new(Point { x: 1, y: 1 }, Point { x: 1, y: -1 });
        assert_eq!(inter, s1.intersection(&s2).expect("Should intersect"));
        assert_eq!(inter, s2.intersection(&s1).expect("Should intersect"));
    }

    #[test]
    fn test_segment_included() {
        let s1 = Segment::new(Point { x: 1, y: 1 }, Point { x: 1, y: -5 });
        let s2 = Segment::new(Point { x: 1, y: 0 }, Point { x: 1, y: -3 });
        assert_eq!(s2, s1.intersection(&s2).expect("Should intersect"));
        assert_eq!(s2, s2.intersection(&s1).expect("Should intersect"));
    }

    #[test]
    fn test_segment_equals() {
        let s1 = Segment::new(Point { x: 1, y: 1 }, Point { x: 1, y: -5 });
        let s2 = s1.clone();
        assert_eq!(s1, s1.intersection(&s2).expect("Should intersect"));
        assert_eq!(s1, s2.intersection(&s1).expect("Should intersect"));
    }

    #[test]
    fn test_segment_iterator() {
        let mut path: Vec<Segment> = Vec::new();
        path.push(Segment::new(Point { x: 1, y: 1 }, Point { x: 1, y: 0 }));
        path.push(Segment::new(Point { x: 5, y: 1 }, Point { x: 7, y: 1 }));
        let points: HashSet<Point> = path.iter().flat_map(|x| x.clone()).collect();
        let mut control: HashSet<Point> = HashSet::new();
        control.insert(Point { x: 1, y: 1 });
        control.insert(Point { x: 1, y: 0 });
        control.insert(Point { x: 5, y: 1 });
        control.insert(Point { x: 6, y: 1 });
        control.insert(Point { x: 7, y: 1 });
        assert_eq!(control, points);
    }

    #[test]
    fn test_path_into_segments() {
        let path = "L1,U1";
        let segs = super::path_into_segments(&path);
        let ps1 = Segment::new(Point { x: -1, y: 0 }, Point { x: 0, y: 0 });
        let ps2 = Segment::new(Point { x: -1, y: 1 }, Point { x: -1, y: 0 });
        let control = vec![ps1, ps2];
        assert_eq!(control, segs);
    }

    #[test]
    fn test_example_1() {
        let wire1 = "R8,U5,L5,D3";
        let wire2 = "U7,R6,D4,L4";
        let paths1 = super::path_into_segments(&wire1);
        let paths2 = super::path_into_segments(&wire2);
        let intersections = super::get_intersections(&paths1, &paths2);
        let min_dist = super::distance_to_closest_intersection(&intersections);
        assert_eq!(6, min_dist);

        let best_dist =
            super::get_best_intersection_in_wire_distance(&paths1, &paths2, &intersections);
        assert_eq!(30, best_dist);
    }

    #[test]
    fn test_example_2() {
        let wire1 = "R75,D30,R83,U83,L12,D49,R71,U7,L72";
        let wire2 = "U62,R66,U55,R34,D71,R55,D58,R83";
        let paths1 = super::path_into_segments(&wire1);
        let paths2 = super::path_into_segments(&wire2);
        let intersections = super::get_intersections(&paths1, &paths2);
        let min_dist = super::distance_to_closest_intersection(&intersections);
        assert_eq!(159, min_dist);

        let best_dist =
            super::get_best_intersection_in_wire_distance(&paths1, &paths2, &intersections);
        assert_eq!(610, best_dist);
    }

    #[test]
    fn test_example_3() {
        let wire1 = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51";
        let wire2 = "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7";
        let paths1 = super::path_into_segments(&wire1);
        let paths2 = super::path_into_segments(&wire2);
        let intersections = super::get_intersections(&paths1, &paths2);
        let min_dist = super::distance_to_closest_intersection(&intersections);
        assert_eq!(135, min_dist);

        let best_dist =
            super::get_best_intersection_in_wire_distance(&paths1, &paths2, &intersections);
        assert_eq!(410, best_dist);
    }
}
