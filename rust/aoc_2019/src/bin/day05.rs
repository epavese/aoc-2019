use aoc_2019::intcode::{IntCodeInterpreter, IntCodeInterpreterError};
use std::env;
use std::fs;

fn main() -> Result<(), IntCodeInterpreterError> {
    let args: Vec<String> = env::args().collect();
    let input_file = args
        .get(1)
        .expect("You need to provide the input file as argument");
    let mem_space: Vec<i64> = fs::read_to_string(input_file)
        .expect("Error reading file")
        .trim()
        .split(',')
        .map(|s| s.parse::<i64>().expect("Error reading u64 from file"))
        .collect();

    let mut interpreter = IntCodeInterpreter::new(mem_space.clone());
    interpreter.set_input(1);
    interpreter.run()?;
    println!(
        "Day 05, part 1: result is {}",
        interpreter.get_output().unwrap()
    );

    let mut interpreter = IntCodeInterpreter::new(mem_space);
    interpreter.set_input(5);
    interpreter.run()?;
    println!(
        "Day 05, part 2: result is {}",
        interpreter.get_output().unwrap()
    );

    Ok(())
}
