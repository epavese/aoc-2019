use std::cmp::Ordering;
use std::env;
use std::fs;
use std::ops::{Add, Sub};

use num::integer;

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
struct SpaceVector {
    pub x: i32,
    pub y: i32,
    pub z: i32,
}

impl std::fmt::Display for SpaceVector {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "({}, {}, {})", self.x, self.y, self.z)
    }
}

impl Add for SpaceVector {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        Self {x: self.x + other.x, y: self.y + other.y, z: self.z + other.z}
    }
}

impl Sub for SpaceVector {
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        Self {x: self.x - other.x, y: self.y - other.y, z: self.z - other.z}
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
struct Moon {
    pub position: SpaceVector,
    pub velocity: SpaceVector,
}

impl std::fmt::Display for Moon {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Pos: {} Vel: {}", self.position, self.velocity)
    }
}

impl Add for Moon {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        Self {position: self.position + other.position, velocity: self.velocity + other.velocity}
    }
}

impl Sub for Moon {
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        Self {position: self.position - other.position, velocity: self.velocity - other.velocity}
    }
}

impl Moon {
    pub fn new(spec: &str) -> Self {
        // spec is <x=X, y=Y, z=Z> where X Y Z are i32-parsable
        let clean_spec = spec.to_owned().replace("<", "").replace(">","").replace("x=", "").replace("y=", "").replace("z=", "").replace(" ","");
        let coords = clean_spec.split(',').collect::<Vec<_>>();
        let position = SpaceVector {x: coords[0].parse::<i32>().unwrap(), y: coords[1].parse::<i32>().unwrap(), z: coords[2].parse::<i32>().unwrap()};
        let velocity = SpaceVector {x: 0, y: 0, z:0};
        Self {position, velocity}
    }

    pub fn move_around(&mut self) {
        self.position = SpaceVector{x: self.position.x + self.velocity.x, y: self.position.y + self.velocity.y, z: self.position.z + self.velocity.z};
    }

    pub fn potential_energy(&self) -> u32 {
        self.position.x.abs() as u32 + self.position.y.abs() as u32 + self.position.z.abs() as u32
    }

    pub fn kinetic_energy(&self) -> u32 {
        self.velocity.x.abs() as u32 + self.velocity.y.abs() as u32 + self.velocity.z.abs() as u32
    }

    pub fn energy(&self) -> u32 {
        self.potential_energy() * self.kinetic_energy()
    }

    pub fn x_config(&self) -> Vec<i32> {
        vec![self.position.x, self.velocity.x]
    }

    pub fn y_config(&self) -> Vec<i32> {
        vec![self.position.y, self.velocity.y]
    }

    pub fn z_config(&self) -> Vec<i32> {
        vec![self.position.z, self.velocity.z]
    }
}

fn calculate_gravities(moons: &[SpaceVector]) -> Vec<SpaceVector> {
    let mut gravities = moons.iter().map(|_| SpaceVector{x: 0, y:0, z: 0}).collect::<Vec<_>>();
    for i in 0..gravities.len() {
        for j in 0..gravities.len() {
            if i == j {continue;}
            match moons[i].x.cmp(&moons[j].x) {
                Ordering::Less => gravities[i].x += 1,
                Ordering::Greater => gravities[i].x -= 1,
                _ => ()
            }
            match moons[i].y.cmp(&moons[j].y) {
                Ordering::Less => gravities[i].y += 1,
                Ordering::Greater => gravities[i].y -= 1,
                _ => ()
            }
            match moons[i].z.cmp(&moons[j].z) {
                Ordering::Less => gravities[i].z += 1,
                Ordering::Greater => gravities[i].z -= 1,
                _ => ()
            }
        }
    }
    gravities
}

fn simulate_motion(moons: &[Moon], steps: u64) -> Vec<Moon> {
    let mut current_moons = moons.to_vec();
    for _ in 0..steps {
        let gravities = calculate_gravities(&current_moons.iter().map(|m| m.position.clone()).collect::<Vec<_>>());
        for i in 0..gravities.len() {
            current_moons[i].velocity = current_moons[i].velocity + gravities[i];
        }

        current_moons.iter_mut().for_each(|m| m.move_around());
    }
    current_moons
}

fn get_loop_time(moons: &[Moon]) -> u64 {
    // may not terminate if it doesn't cycle
    let mut current_moons = moons.to_vec();
    let x_init: Vec<_> = current_moons.iter().map(Moon::x_config).collect();
    let y_init: Vec<_> = current_moons.iter().map(Moon::y_config).collect();
    let z_init: Vec<_> = current_moons.iter().map(Moon::z_config).collect();
    let (mut count_x, mut count_y, mut count_z, mut steps) = (0 as u64, 0 as u64, 0 as u64, 0 as u64);
    while [count_x, count_y, count_z].contains(&0) {
        steps += 1;
        current_moons = simulate_motion(&current_moons, 1);
        let x_cur: Vec<_> = current_moons.iter().map(Moon::x_config).collect();
        let y_cur: Vec<_> = current_moons.iter().map(Moon::y_config).collect();
        let z_cur: Vec<_> = current_moons.iter().map(Moon::z_config).collect();
        if count_x == 0 && x_cur == x_init {
            count_x = steps;
        }
        if count_y == 0 && y_cur == y_init {
            count_y = steps;
        }
        if count_z == 0 && z_cur == z_init {
            count_z = steps;
        }
    }

    integer::lcm(count_x, num::integer::lcm(count_y, count_z))
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let input_file = args
        .get(1)
        .expect("You need to provide the input file as argument");

    let moons = fs::read_to_string(input_file)
        .expect("Error reading file")
        .trim()
        .split('\n')
        .map(|s| Moon::new(s))
        .collect::<Vec<_>>();

    let moved_moons = simulate_motion(&moons, 1000);
    println!(
        "Day 05, part 1: result is {}", moved_moons.iter().map(Moon::energy).sum::<u32>()
    );
    println!(
        "Day 05, part 2: result is {}", get_loop_time(&moons)
    );
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_energy_10_steps() {
        let m1 = Moon::new("<x=-1, y=0, z=2>");
        let m2 = Moon::new("<x=2, y=-10, z=-7>");
        let m3 = Moon::new("<x=4, y=-8, z=8>");
        let m4 = Moon::new("<x=3, y=5, z=-1>");
        let moons = vec![m1, m2, m3, m4];

        let moved_twice = simulate_motion(&moons, 2);
        let mut moved_and_moved = simulate_motion(&moons, 1);
        moved_and_moved = simulate_motion(&moved_and_moved, 1);
        assert_eq!(moved_twice, moved_and_moved);

        let moved_moons = simulate_motion(&moons, 10);
        assert_eq!(179 as u32, moved_moons.iter().map(Moon::energy).sum());
    }

    #[test]
    fn test_energy_100_steps() {
        let m1 = Moon::new("<x=-8, y=-10, z=0>");
        let m2 = Moon::new("<x=5, y=5, z=10>");
        let m3 = Moon::new("<x=2, y=-7, z=3>");
        let m4 = Moon::new("<x=9, y=-8, z=-3>");

        let moons = vec![m1, m2, m3, m4];
        let moved_moons = simulate_motion(&moons, 100);
        assert_eq!(1940 as u32, moved_moons.iter().map(Moon::energy).sum());
    }

    #[test]
    fn test_repetition() {
        let m1 = Moon::new("<x=-1, y=0, z=2>");
        let m2 = Moon::new("<x=2, y=-10, z=-7>");
        let m3 = Moon::new("<x=4, y=-8, z=8>");
        let m4 = Moon::new("<x=3, y=5, z=-1>");
        let moons = vec![m1, m2, m3, m4];

        assert_eq!(2772, get_loop_time(&moons));
    }
    
    #[test]
    fn test_long_repetition() {
        let m1 = Moon::new("<x=-8, y=-10, z=0>");
        let m2 = Moon::new("<x=5, y=5, z=10>");
        let m3 = Moon::new("<x=2, y=-7, z=3>");
        let m4 = Moon::new("<x=9, y=-8, z=-3>");
        let moons = vec![m1, m2, m3, m4];

        assert_eq!(4686774924, get_loop_time(&moons));
    }
}
