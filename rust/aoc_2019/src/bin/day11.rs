use aoc_2019::intcode::{IntCodeInterpreter, IntCodeInterpreterError, IntCodeInterpreterStatus};
use std::cmp::Ordering;
use std::collections::HashSet;
use std::convert::TryFrom;
use std::env;
use std::fs;
use std::iter::FromIterator;

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
struct Position {
    pub x: i32,
    pub y: i32,
}

#[derive(Debug, Eq, PartialEq)]
enum Heading {
    Up,
    Right,
    Down,
    Left,
}

#[derive(Debug, Eq, PartialEq)]
enum Color {
    Black,
    White,
}

impl TryFrom<i64> for Color {
    type Error = ();
    fn try_from(val: i64) -> Result<Self, Self::Error> {
        match val {
            0 => Ok(Self::Black),
            1 => Ok(Self::White),
            _ => Err(()),
        }
    }
}

fn update_heading(heading: &Heading, code: &i64) -> Heading {
    match (heading, code) {
        (Heading::Up, 0) | (Heading::Down, 1) => Heading::Left,
        (Heading::Up, 1) | (Heading::Down, 0) => Heading::Right,
        (Heading::Right, 0) | (Heading::Left, 1) => Heading::Up,
        (Heading::Right, 1) | (Heading::Left, 0) => Heading::Down,
        _ => unreachable!(),
    }
}

fn advance(position: &Position, heading: &Heading) -> Position {
    match heading {
        Heading::Up => Position {
            x: position.x,
            y: position.y - 1,
        },
        Heading::Right => Position {
            x: position.x + 1,
            y: position.y,
        },
        Heading::Down => Position {
            x: position.x,
            y: position.y + 1,
        },
        Heading::Left => Position {
            x: position.x - 1,
            y: position.y,
        },
    }
}

struct PaintingRobot {
    pub position: Position,
    pub heading: Heading,
    painted_white: HashSet<Position>,
    painted_cells: HashSet<Position>,
    program: IntCodeInterpreter,
}

impl PaintingRobot {
    pub fn new(prg: IntCodeInterpreter) -> Self {
        Self {
            position: Position { x: 0, y: 0 },
            heading: Heading::Up,
            painted_white: HashSet::new(),
            painted_cells: HashSet::new(),
            program: prg,
        }
    }

    pub fn display_wall(&self) -> String {
        let mut white_cells = Vec::from_iter(&self.painted_white);
        white_cells.sort_by(|pos1, pos2| {
            if pos1 == pos2 {
                Ordering::Equal
            } else if pos1.y < pos2.y {
                Ordering::Less
            } else if pos1.y > pos2.y {
                Ordering::Greater
            } else {
                pos1.x.cmp(&pos2.x)
            }
        });

        if !white_cells.is_empty() {
            let (min_y, max_y) = (white_cells[0].y, white_cells[white_cells.len() - 1].y);
            let (min_x, max_x) =
                white_cells
                    .iter()
                    .fold((std::i32::MAX, std::i32::MIN), |(small, big), pos| {
                        (
                            if small < pos.x { small } else { pos.x },
                            if big > pos.x { big } else { pos.x },
                        )
                    });
            let mut result = String::from("");
            let mut white_ndx = 0;
            for y in min_y..=max_y {
                for x in min_x..=max_x {
                    if white_ndx < white_cells.len()
                        && white_cells[white_ndx].x == x
                        && white_cells[white_ndx].y == y
                    {
                        result.push('#');
                        white_ndx += 1;
                    } else {
                        result.push('.');
                    }
                }
                result.push('\n');
            }
            result
        } else {
            "\n".to_owned()
        }
    }

    pub fn paint_cell(&mut self, position: Position, color: Color) {
        self.painted_cells.insert(self.position);
        if color == Color::White {
            self.painted_white.insert(position);
        } else {
            self.painted_white.remove(&position);
        }
    }

    pub fn paint(&mut self) {
        loop {
            match self.program.run_concurrent() {
                Ok(IntCodeInterpreterStatus::Done) => return (),
                Ok(IntCodeInterpreterStatus::AwaitingInput) => {
                    // provide color input, run twice to expect color to paint and movement
                    self.program
                        .set_input(if self.painted_white.contains(&self.position) {
                            1
                        } else {
                            0
                        });
                    let status = self.program.run_concurrent();
                    assert_eq!(status.unwrap(), IntCodeInterpreterStatus::EmittedOutput);
                    let next_color = self.program.get_output().unwrap();

                    let status = self.program.run_concurrent();
                    assert_eq!(status.unwrap(), IntCodeInterpreterStatus::EmittedOutput);
                    let maneuver = self.program.get_output().unwrap();

                    self.paint_cell(self.position, Color::try_from(next_color).unwrap());
                    self.heading = update_heading(&self.heading, &maneuver);
                    self.position = advance(&self.position, &self.heading);
                }
                _ => unreachable!(),
            }
        }
    }
}

fn main() -> Result<(), IntCodeInterpreterError> {
    let args: Vec<String> = env::args().collect();
    let input_file = args
        .get(1)
        .expect("You need to provide the input file as argument");
    let mem_space: Vec<i64> = fs::read_to_string(input_file)
        .expect("Error reading file")
        .trim()
        .split(',')
        .map(|s| s.parse::<i64>().expect("Error reading i64 from file"))
        .collect();

    let mut interpreter = IntCodeInterpreter::new(mem_space.clone());
    let mut painter = PaintingRobot::new(interpreter);
    painter.paint();

    println!("Day 11, part 1: result is {}", painter.painted_cells.len());

    interpreter = IntCodeInterpreter::new(mem_space);
    painter = PaintingRobot::new(interpreter);
    painter.paint_cell(Position { x: 0, y: 0 }, Color::White);
    painter.paint();
    println!("Day 11, part 2: result is \n{}", painter.display_wall());

    Ok(())
}
