use std::env;
use std::fs;

const COLUMNS: usize = 25;
const ROWS: usize = 6;

fn main() {
    let args: Vec<String> = env::args().collect();
    let input_file = args
        .get(1)
        .expect("You need to provide the input file as argument");
    let mut image_data = fs::read_to_string(input_file)
        .expect("Error reading input file")
        .trim()
        .to_owned();

    let mut min_zeroes = COLUMNS * ROWS + 1;
    let mut result = 0;
    let mut visible_layer = image_data[0..COLUMNS * ROWS].to_owned();
    while !image_data.is_empty() {
        let (layer, rest) = image_data.split_at_mut(COLUMNS * ROWS);
        let (mut zeroes, mut ones, mut twos) = (0, 0, 0);
        layer.chars().for_each(|x| {
            assert!(x == '0' || x == '1' || x == '2');
            if x == '0' {
                zeroes += 1;
            }
            if x == '1' {
                ones += 1;
            }
            if x == '2' {
                twos += 1;
            }
        });

        if zeroes < min_zeroes {
            min_zeroes = zeroes;
            result = ones * twos;
        }

        visible_layer = calculate_overlay(visible_layer, &layer);
        image_data = rest.to_owned();
    }

    println!("Day 08, part 1: {}", result);
    let mut ndx = 0;
    println!("Day 08, part 2:");
    for _i in 0..ROWS {
        println!(
            "{}",
            visible_layer[ndx..ndx + COLUMNS]
                .chars()
                .map(|x| if x == '1' { 'X' } else { ' ' })
                .collect::<String>()
        );
        ndx += COLUMNS;
    }
}

fn calculate_overlay(visible: String, layer: &str) -> String {
    visible
        .chars()
        .zip(layer.chars())
        .map(|(top, bottom)| if top == '2' { bottom } else { top })
        .collect()
}
