use std::collections::HashMap;
use std::collections::HashSet;
use std::env;
use std::fs;

fn build_universe<'a>(orbits: &[(&'a str, &'a str)]) -> HashMap<&'a str, HashSet<&'a str>> {
    let mut universe = HashMap::new();

    for (object, orbiter) in orbits {
        universe
            .entry(*object)
            .and_modify(|o: &mut HashSet<_>| {
                (*o).insert(*orbiter);
            })
            .or_insert_with(|| {
                let mut s = HashSet::new();
                s.insert(*orbiter);
                s
            });
    }

    universe
}

fn calculate_orbits(universe: &HashMap<&str, HashSet<&str>>) -> u32 {
    let mut total = 0;
    let mut queue: Vec<(&str, u32)> = Vec::new();

    queue.push(("COM", 0));
    while let Some((object, orbits)) = queue.pop() {
        total += orbits;
        universe.get(object).and_then(|orbiters| {
            for obj in orbiters {
                queue.push((obj, orbits + 1));
            }
            Some(())
        });
    }

    total
}

fn reverse_map<'a>(universe: &HashMap<&'a str, HashSet<&'a str>>) -> HashMap<&'a str, &'a str> {
    let mut queue: Vec<&str> = Vec::new();
    let mut reverse: HashMap<&str, &str> = HashMap::new();

    queue.push("COM");
    while let Some(object) = queue.pop() {
        universe.get(object).and_then(|orbiters| {
            for obj in orbiters {
                queue.push(obj);
                reverse.insert(obj, object);
            }
            Some(())
        });
    }

    reverse
}

fn path_to<'a>(reverse_universe: &HashMap<&'a str, &'a str>, object: &str) -> Vec<&'a str> {
    let mut path: Vec<&str> = Vec::new();
    let mut obj = object;

    while let Some(parent) = reverse_universe.get(obj) {
        path.push(parent);
        obj = parent;
    }

    path
}

fn orbital_transfers(reverse_universe: &HashMap<&str, &str>, obj1: &str, obj2: &str) -> usize {
    let path_to_1 = path_to(&reverse_universe, obj1);
    let path_to_2 = path_to(&reverse_universe, obj2);

    let mut i = path_to_1.len() - 1;
    let mut j = path_to_2.len() - 1;
    while path_to_1[i] == path_to_2[j] {
        i -= 1;
        j -= 1;
    }

    i + j + 2
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let input_file = args
        .get(1)
        .expect("You need to provide the input file as argument");
    let data = fs::read_to_string(input_file).expect("Error reading file");
    let orbits: Vec<(&str, &str)> = data
        .trim()
        .split_whitespace()
        .map(|x| {
            let mut it = x.split(')');
            (it.next().unwrap(), it.next().unwrap())
        })
        .collect();

    let universe = build_universe(&orbits);
    println!("Day 06, part 1: {} orbits", calculate_orbits(&universe));

    let reverse = reverse_map(&universe);

    println!(
        "Day 06, part 2: {} orbital transfers",
        orbital_transfers(&reverse, "YOU", "SAN")
    );
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_tiny_linear() {
        let data = vec![("COM", "P1"), ("P1", "P2"), ("P2", "P3")];

        let universe = super::build_universe(&data);
        assert_eq!(6, super::calculate_orbits(&universe));
    }

    #[test]
    fn test_tiny_linear_reverse() {
        let data = vec![("P2", "P3"), ("P1", "P2"), ("COM", "P1")];

        let universe = super::build_universe(&data);
        assert_eq!(6, super::calculate_orbits(&universe));
    }

    #[test]
    fn test_small_universe() {
        let data = vec![
            ("COM", "B"),
            ("B", "C"),
            ("C", "D"),
            ("D", "E"),
            ("E", "F"),
            ("B", "G"),
            ("G", "H"),
            ("D", "I"),
            ("E", "J"),
            ("J", "K"),
            ("K", "L"),
        ];

        let universe = super::build_universe(&data);
        assert_eq!(42, super::calculate_orbits(&universe));
    }

    #[test]
    fn test_orbital_transfers() {
        let data = vec![
            ("COM", "B"),
            ("B", "C"),
            ("C", "D"),
            ("D", "E"),
            ("E", "F"),
            ("B", "G"),
            ("G", "H"),
            ("D", "I"),
            ("E", "J"),
            ("J", "K"),
            ("K", "L"),
            ("K", "YOU"),
            ("I", "SAN"),
        ];

        let universe = super::build_universe(&data);
        let reverse = super::reverse_map(&universe);

        assert_eq!(4, super::orbital_transfers(&reverse, "SAN", "YOU"));
    }
}
