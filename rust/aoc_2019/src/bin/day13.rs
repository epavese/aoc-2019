use aoc_2019::intcode::{IntCodeInterpreter, IntCodeInterpreterError, IntCodeInterpreterStatus};
use std::collections::{HashMap, HashSet};
use std::env;
use std::fs;
use std::io::{stdin, stdout};

use termion::raw::IntoRawMode;

enum Texture {
    Wall,
    Block,
    Paddle,
    Ball
}

struct BlockBreaker<R: std::io::Read + termion::input::TermRead, W: std::io::Write> {
    game: IntCodeInterpreter,
    score: i64,
    textures: HashMap<(u16, u16), Texture>,

    screen: W,
    #[allow(dead_code)]
    input: R,
}

impl<R: std::io::Read + termion::input::TermRead, W: std::io::Write> BlockBreaker<R, W> {
    pub fn new(game: IntCodeInterpreter, io_in: R, io_out: W) -> BlockBreaker<R, termion::raw::RawTerminal<W>> {
        BlockBreaker {
            game,
            score: 0,
            textures: HashMap::new(),
            screen: io_out.into_raw_mode().unwrap(),
            input: io_in,
        }
    }

    pub fn run(&mut self) -> Result<(), ()> {
        self.game.write_mem(0, 2).expect("Problem running game");
        loop {
            let status = self.game.run_concurrent().expect("Problem running the program!");
            match status {
                IntCodeInterpreterStatus::EmittedOutput => {
                    // get two more values
                    self.game.run_concurrent().expect("Expecting more output");
                    self.game.run_concurrent().expect("Expecting more output");
                    let x = self.game.get_output().unwrap();
                    let y = self.game.get_output().unwrap();
                    let val = self.game.get_output().unwrap();
                    match (x, y, val) {
                        (-1, 0, _) => { self.score = val; }
                        (_, _, 0) => { self.textures.remove(&(x as u16, y as u16)); }
                        (_, _, 1) => { self.textures.insert((x as u16, y as u16), Texture::Wall); }
                        (_, _, 2) => { self.textures.insert((x as u16, y as u16), Texture::Block); }
                        (_, _, 3) => { self.textures.insert((x as u16, y as u16), Texture::Paddle); }
                        (_, _, 4) => { self.textures.insert((x as u16, y as u16), Texture::Ball); }
                        _ => unreachable!(),
                    }
                },
                IntCodeInterpreterStatus::AwaitingInput => {
                    self.draw_frame();
                    /*
                    loop {
                        let mut b = [0];
                        self.input.read(&mut b).unwrap();
                        match b[0] {
                            b'd' => {self.game.set_input(1); break;},
                            b'a' => {self.game.set_input(-1); break;},
                            b's' | b'w' => {self.game.set_input(0); break;},
                            _ => (),
                        }
                    }
                    */
                    // using hacked version, do nothing
                    self.game.set_input(0);
                },
                IntCodeInterpreterStatus::Done => { self.draw_frame(); self.clear(); break; },
            }
        }

        Ok(())
    }
    
    fn draw_frame(&mut self) {
        write!(self.screen, "{}", termion::clear::All).unwrap();
        for (k, v) in self.textures.iter() {
            write!(self.screen, "{}{}{}", termion::cursor::Hide, termion::cursor::Goto(k.0*3+1, (k.1)+1), match v {
                Texture::Wall => "|||",
                Texture::Block => "XXX",
                Texture::Paddle => "===",
                Texture::Ball => "[o]",
            }).unwrap();
        }

        write!(self.screen, "{}Score = {}", termion::cursor::Goto(1, 35), self.score).unwrap();
        self.screen.flush().expect("Error flushing screen");
    }

    fn clear(&mut self) {
        write!(self.screen, "{}{}", termion::cursor::Goto(1, 40), termion::cursor::Show).unwrap();
    }
}

fn main() -> Result<(), IntCodeInterpreterError> {
    let args: Vec<String> = env::args().collect();
    let input_file = args
        .get(1)
        .expect("You need to provide the input file as argument");
    let mem_space: Vec<i64> = fs::read_to_string(input_file)
        .expect("Error reading file")
        .trim()
        .split(',')
        .map(|s| s.parse::<i64>().expect("Error reading i64 from file"))
        .collect();

    let mut interpreter = IntCodeInterpreter::new(mem_space.clone());
    interpreter.run().expect("Should not fail");

    let mut blocks: HashSet<(i64,i64)> = HashSet::new();
    loop {
        if let Some(x) = interpreter.get_output() {
            let y = interpreter.get_output().unwrap();
            let tile = interpreter.get_output().unwrap();

            match tile {
                2 => blocks.insert((x,y)),
                _ => blocks.remove(&(x,y)),
            };
        } else {
            break;
        }
    }

    println!("Day 13, part 1: result is {}", blocks.len());

    println!("Day 13, part 2: result is ?");
    let io_in = stdin();
    let io_out = stdout();
    let mut game = BlockBreaker::new(IntCodeInterpreter::new(mem_space), io_in.lock(), io_out.lock());
    game.run().unwrap();

    Ok(())
}
