use std::env;

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() < 3 {
        panic!("Please specify the start and end of the range");
    }

    let start = args
        .get(1)
        .unwrap()
        .parse::<u32>()
        .expect("Error parsing start of range");
    let end = args
        .get(2)
        .unwrap()
        .parse::<u32>()
        .expect("Error parsing end of range");

    let n_pass: u32 = (start..end + 1)
        .map(|x| check_password(x, false) as u32)
        .sum();
    println!("Day 04, part 1: {} passwords", n_pass);

    let n_pass: u32 = (start..end + 1)
        .map(|x| check_password(x, true) as u32)
        .sum();
    println!("Day 04, part 2: {} passwords", n_pass);
}

fn check_password(passwd: u32, v2: bool) -> bool {
    let pass_str = passwd.to_string();
    let mut prev: Option<char> = None;
    let mut has_dup: bool = false;
    let mut increasing = true;
    let mut rep_count = 1;

    for c in pass_str.chars() {
        if let Some(x) = prev {
            if x == c {
                rep_count += 1;
            }

            if rep_count == 2 && (!v2 || x != c) {
                has_dup = true;
            }

            if x != c {
                rep_count = 1;
            }

            if x.to_digit(10) > c.to_digit(10) {
                increasing = false;
                break;
            }
        }

        prev = Some(c);
    }

    increasing && (has_dup || rep_count == 2)
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_simple_passwords_v1() {
        assert!(super::check_password(111111, false));
        assert!(!super::check_password(223450, false));
        assert!(!super::check_password(123789, false));
    }

    #[test]
    fn test_simple_passwords_v2() {
        assert!(super::check_password(112233, true));
        assert!(!super::check_password(123444, true));
        assert!(super::check_password(111122, true));
    }
}
