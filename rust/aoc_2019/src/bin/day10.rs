use std::cmp::Ordering;
use std::collections::HashMap;
use std::env;
use std::fs;

use rug::Rational;

// There is just the single quadrant
#[derive(Debug, Eq, Hash, PartialEq)]
struct Position {
    pub x: usize,
    pub y: usize,
}

impl std::fmt::Display for Position {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[derive(Debug, Eq, Hash, PartialEq)]
enum Direction {
    Positive,
    Negative,
}

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd)]
enum Quadrant {
    BoundaryQ4Q1,
    Q1,
    BoundaryQ1Q2,
    Q2,
    BoundaryQ2Q3,
    Q3,
    BoundaryQ3Q4,
    Q4,
}

// Raytrace from a point.
// Given by a slope and direction.
// Assuming quadrants from viewer position are as follows
// ___.___
//  4 | 1
// ___|___
//  3 | 2
// ___|___
//
// Starting clockwise from Q4-Q1 boundary:
// Q4-Q1 boundary has None slope, positive direction
// Q1 has positive slope, positive direction
// Q1-Q2 boundary has 0 slope, positive direction
// Q2 has negative slope, positive direction
// Q2-Q3 boundary has None slope, negative direction
// Q3 has positive slope, negative direction
// Q3-Q4 boundary has 0 slope, negative direction
// Q4 has negative slope, negative direction
//
// Note (0,0) is top left and y grows DOWN
#[derive(Debug, Eq, Hash, PartialEq)]
struct Ray {
    pub slope: Option<Rational>,
    pub direction: Direction,
}

impl Ray {
    pub fn new(from: &Position, to: &Position) -> Self {
        assert!(*from != *to);
        let slope = if to.x == from.x {
            None
        } else {
            Some(Rational::from((
                to.y as i32 - from.y as i32,
                to.x as i32 - from.x as i32,
            )))
        };

        let direction = if to.x > from.x || (to.x == from.x && to.y < from.y) {
            Direction::Positive
        } else {
            Direction::Negative
        };

        Ray { slope, direction }
    }

    pub fn quadrant(&self) -> Quadrant {
        if let Some(slp) = &self.slope {
            match (slp.cmp0(), &self.direction) {
                (Ordering::Equal, Direction::Positive) => Quadrant::BoundaryQ1Q2,
                (Ordering::Equal, Direction::Negative) => Quadrant::BoundaryQ3Q4,
                (Ordering::Less, Direction::Positive) => Quadrant::Q1,
                (Ordering::Less, Direction::Negative) => Quadrant::Q3,
                (Ordering::Greater, Direction::Positive) => Quadrant::Q2,
                (Ordering::Greater, Direction::Negative) => Quadrant::Q4,
            }
        } else if self.direction == Direction::Positive {
            Quadrant::BoundaryQ4Q1
        } else {
            Quadrant::BoundaryQ2Q3
        }
    }
}

// ordering starts at boundary between 1 and 2 and continues clockwise
impl Ord for Ray {
    fn cmp(&self, other: &Self) -> Ordering {
        if self == other {
            Ordering::Equal
        } else if self.quadrant() != other.quadrant() {
            self.quadrant().cmp(&other.quadrant())
        } else {
            self.slope
                .as_ref()
                .unwrap()
                .cmp(other.slope.as_ref().unwrap())
        }
    }
}

impl PartialOrd for Ray {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

type RayScan<'a> = HashMap<Ray, Vec<&'a Position>>;
type ViewMap<'a> = HashMap<&'a Position, RayScan<'a>>;

fn build_view_map(asteroids: &[Position]) -> ViewMap {
    // the inner vectors are ordered by position distance to the map key
    let mut result = ViewMap::new();
    asteroids.iter().for_each(|pos| {
        result.insert(pos, build_asteroid_view(asteroids, pos));
    });
    result
}

fn build_asteroid_view<'a>(asteroids: &'a [Position], center: &Position) -> RayScan<'a> {
    let mut asteroid_view = asteroids
        .iter()
        .filter(|&pos| pos != center)
        .map(|pos| (Ray::new(center, pos), pos))
        .fold(RayScan::new(), |mut accum, (ray, pos)| {
            if let Some(seen) = accum.get_mut(&ray) {
                seen.push(pos);
            } else {
                accum.insert(ray, vec![pos]);
            }
            accum
        });

    // now it remains to sort the vectors by distance to center
    asteroid_view.iter_mut().for_each(|(_, asters)| {
        asters.sort_by(|pos1, pos2| {
            distance(center, pos1)
                .partial_cmp(&distance(center, pos2))
                .unwrap()
        })
    });
    asteroid_view
}

fn distance(from: &Position, to: &Position) -> f32 {
    // not checking numerical bounds at all
    let x_dist = from.x as f32 - to.x as f32;
    let y_dist = from.y as f32 - to.y as f32;
    (x_dist * x_dist + y_dist * y_dist).sqrt()
}

fn parse_asteroids(text: &str) -> Vec<Position> {
    text.split('\n')
        .enumerate()
        .flat_map(|(y, row)| row.char_indices().map(move |(x, item)| (x, y, item)))
        .filter_map(|(x, y, item)| {
            if item == '#' {
                Some(Position { x, y })
            } else {
                None
            }
        })
        .collect::<Vec<_>>()
}

fn vaporized_at<'a>(view_map: &'a ViewMap, from: &Position, nth: usize) -> &'a Position {
    let laser_station_view = view_map.get(from).unwrap();
    let mut sorted_rays = laser_station_view.keys().collect::<Vec<_>>();
    sorted_rays.sort();
    let mut rays_iterators = sorted_rays
        .iter()
        .map(|ray| laser_station_view.get(ray).unwrap().iter())
        .collect::<Vec<_>>();

    let mut vaporized = None;
    let mut ndx = 0;
    let mut nth = nth;
    while nth > 0 {
        let next_asteroid = rays_iterators[ndx].next();
        if next_asteroid.is_some() {
            vaporized = next_asteroid;
            nth -= 1;
        }
        ndx = (ndx + 1) % rays_iterators.len();
    }
    vaporized.unwrap()
}

// type RayScan<'a> = HashMap<Ray, Vec<&'a Position>>;
// type ViewMap<'a> = HashMap<&'a Position, RayScan<'a>>;
fn main() -> Result<(), ()> {
    let args: Vec<String> = env::args().collect();
    let input_file = args
        .get(1)
        .expect("You need to provide the input file as argument");
    let asteroids: Vec<Position> =
        parse_asteroids(&fs::read_to_string(input_file).expect("Error reading file"));
    let view_map = build_view_map(&asteroids);
    let (best_pos, view_from_pos) = view_map
        .iter()
        .max_by(|(_, view1), (_, view2)| view1.len().cmp(&view2.len()))
        .unwrap();
    println!(
        "Day 09, part 1: result is {} at {}",
        view_from_pos.len(),
        best_pos
    );

    let vaporized = vaporized_at(&view_map, best_pos, 200);
    println!("Day 09, part 2: result is {:?}", vaporized);

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_ray_calculation_and_ordering() {
        let p1 = Position { x: 3, y: 0 };
        let p2 = Position { x: 3, y: 10 };
        let p3 = Position { x: 4, y: 1 };
        let p4 = Position { x: 1, y: 1 };

        let p1_p2 = Ray::new(&p1, &p2);
        assert!(p1_p2.slope.is_none());
        assert_eq!(&Direction::Negative, &p1_p2.direction);
        assert_eq!(Quadrant::BoundaryQ2Q3, p1_p2.quadrant());

        let p2_p1 = Ray::new(&p2, &p1);
        assert!(p2_p1.slope.is_none());
        assert_eq!(&Direction::Positive, &p2_p1.direction);
        assert_eq!(Quadrant::BoundaryQ4Q1, p2_p1.quadrant());

        let p1_p3 = Ray::new(&p1, &p3);
        assert_eq!(&Rational::from(1), p1_p3.slope.as_ref().unwrap());
        assert_eq!(&Direction::Positive, &p1_p3.direction);
        assert_eq!(Quadrant::Q2, p1_p3.quadrant());

        let p3_p1 = Ray::new(&p3, &p1);
        assert_eq!(&Rational::from(1), p3_p1.slope.as_ref().unwrap());
        assert_eq!(&Direction::Negative, &p3_p1.direction);
        assert_eq!(Quadrant::Q4, p3_p1.quadrant());

        let p1_p4 = Ray::new(&p1, &p4);
        assert_eq!(&Rational::from((-1, 2)), p1_p4.slope.as_ref().unwrap());
        assert_eq!(&Direction::Negative, &p1_p4.direction);
        assert_eq!(Quadrant::Q3, p1_p4.quadrant());

        let p4_p1 = Ray::new(&p4, &p1);
        assert_eq!(&Rational::from((-1, 2)), p4_p1.slope.as_ref().unwrap());
        assert_eq!(&Direction::Positive, &p4_p1.direction);
        assert_eq!(Quadrant::Q1, p4_p1.quadrant());
    }

    #[test]
    pub fn test_vaporization() {
        let spacemap = ".#....#####...#..\n\
             ##...##.#####..##\n\
             ##...#...#.#####.\n\
             ..#.....#...###..\n\
             ..#.#.....#....##";
        let asteroids: Vec<Position> = parse_asteroids(spacemap);
        let view_map = build_view_map(&asteroids);
        let laser = Position { x: 8, y: 3 };
        let vaporized = vaporized_at(&view_map, &laser, 1);
        println!("{}", vaporized);
        let vaporized = vaporized_at(&view_map, &laser, 2);
        println!("{}", vaporized);
    }
}
