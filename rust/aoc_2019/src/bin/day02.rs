use aoc_2019::intcode::{IntCodeInterpreter, IntCodeInterpreterError};
use std::env;
use std::fs;

fn main() -> Result<(), IntCodeInterpreterError> {
    let args: Vec<String> = env::args().collect();
    let input_file = args
        .get(1)
        .expect("You need to provide the input file as argument");
    let mem_space: Vec<i64> = fs::read_to_string(input_file)
        .expect("Error reading file")
        .trim()
        .split(',')
        .map(|s| s.parse::<i64>().expect("Error reading u64 from file"))
        .collect();

    let mut interpreter = IntCodeInterpreter::new(mem_space.clone());
    interpreter.write_mem(1, 12)?;
    interpreter.write_mem(2, 2)?;

    interpreter.run()?;
    println!("Day 02, part 1: result is {}", interpreter.read_mem(0)?);

    let res = IntCodeInterpreter::get_inputs_for_output(mem_space, 19_690_720)?;
    println!("Day 02, part 2: result is {}", res.0 * 100 + res.1);

    Ok(())
}
