use std::collections::HashMap;
use std::collections::VecDeque;
use std::convert::TryFrom;

#[derive(Debug)]
pub struct IntCodeInterpreter {
    mem_space: Vec<i64>,
    extra_memory: HashMap<usize, i64>,
    modes: Vec<ParameterMode>,
    input_buf: VecDeque<i64>,
    output_buf: VecDeque<i64>,
    ip: usize,
    relative_base: i64,
    finished: bool,
}

#[derive(Debug, Eq, PartialEq)]
pub enum IntCodeInterpreterStatus {
    Done,
    AwaitingInput,
    EmittedOutput,
}

#[derive(Debug)]
pub struct IntCodeInterpreterError {
    msg: String,
}

#[derive(Debug, Eq, PartialEq)]
enum ParameterMode {
    Position,
    Immediate,
    Relative,
}

impl std::convert::TryFrom<i64> for ParameterMode {
    type Error = String;

    fn try_from(n: i64) -> Result<Self, Self::Error> {
        match n {
            0 => Ok(Self::Position),
            1 => Ok(Self::Immediate),
            2 => Ok(Self::Relative),
            _ => Err(format!("Unknown parameter mode {}", n)),
        }
    }
}

impl std::error::Error for IntCodeInterpreterError {}

impl std::fmt::Display for IntCodeInterpreterError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        f.write_str(&self.msg)
    }
}

impl IntCodeInterpreter {
    pub fn new(mem_space: Vec<i64>) -> IntCodeInterpreter {
        IntCodeInterpreter {
            mem_space,
            extra_memory: HashMap::new(),
            ip: 0,
            input_buf: VecDeque::new(),
            output_buf: VecDeque::new(),
            modes: vec![
                ParameterMode::Position,
                ParameterMode::Position,
                ParameterMode::Position,
            ],
            relative_base: 0,
            finished: false,
        }
    }

    pub fn read_mem(&mut self, pos: usize) -> Result<i64, IntCodeInterpreterError> {
        if pos < self.mem_space.len() {
            Ok(*self
                .mem_space
                .get(pos)
                .ok_or_else(|| IntCodeInterpreterError {
                    msg: format!(
                        "Tried to read from mem position {} / {}",
                        pos,
                        self.mem_space.len()
                    ),
                })?)
        } else {
            Ok(*self.extra_memory.entry(pos).or_insert(0))
        }
    }

    pub fn write_mem(&mut self, pos: usize, val: i64) -> Result<(), IntCodeInterpreterError> {
        if pos < self.mem_space.len() {
            self.mem_space[pos] = val;
        } else {
            self.extra_memory.insert(pos, val);
        }
        Ok(())
    }

    pub fn run_concurrent(&mut self) -> Result<IntCodeInterpreterStatus, IntCodeInterpreterError> {
        loop {
            match self.step()? {
                IntCodeInterpreterStatus::Done if self.finished => {
                    return Ok(IntCodeInterpreterStatus::Done)
                }
                IntCodeInterpreterStatus::Done => (),
                other => return Ok(other),
            }
        }
    }

    pub fn run(&mut self) -> Result<IntCodeInterpreterStatus, IntCodeInterpreterError> {
        loop {
            match self.step()? {
                IntCodeInterpreterStatus::Done if self.finished => {
                    return Ok(IntCodeInterpreterStatus::Done)
                }
                _ => (),
            }
        }
    }

    pub fn set_input(&mut self, value: i64) {
        self.input_buf.push_back(value);
    }

    pub fn get_output(&mut self) -> Option<i64> {
        self.output_buf.pop_front()
    }

    /// gets the n_th parameter based on the current ip and current mode
    fn get_parameter_for_pos(&mut self, index: usize) -> Result<i64, IntCodeInterpreterError> {
        let read = self.read_mem(self.ip + index + 1);
        match self.modes[index] {
            ParameterMode::Immediate => read,
            ParameterMode::Position => self.read_mem(read? as usize),
            ParameterMode::Relative => self.read_mem((read? + self.relative_base) as usize),
        }
    }

    fn clear_modes(&mut self) {
        self.modes = vec![
            ParameterMode::Position,
            ParameterMode::Position,
            ParameterMode::Position,
        ];
    }

    fn set_modes(&mut self, opcode: i64) -> i64 {
        self.modes[0] = ParameterMode::try_from((opcode / 100) % 10)
            .unwrap_or_else(|_| panic!("Invalid opcode {}", opcode));
        self.modes[1] = ParameterMode::try_from((opcode / 1_000) % 10)
            .unwrap_or_else(|_| panic!("Invalid opcode {}", opcode));
        self.modes[2] = ParameterMode::try_from((opcode / 10_000) % 10)
            .unwrap_or_else(|_| panic!("Invalid opcode {}", opcode));
        opcode % 100
    }

    fn step(&mut self) -> Result<IntCodeInterpreterStatus, IntCodeInterpreterError> {
        let opcode = self.read_mem(self.ip)?;
        let pure_opcode = self.set_modes(opcode);
        let mut fail = false;
        match pure_opcode {
            1 | 2 => {
                // The problem statement is confusing in the output parameter
                let offset = if self.modes[2] == ParameterMode::Relative {
                    self.relative_base
                } else {
                    0
                };
                self.modes[2] = ParameterMode::Immediate;
                let save_at = (offset + self.get_parameter_for_pos(2)?) as usize;
                let op1 = self.get_parameter_for_pos(0)?;
                let op2 = self.get_parameter_for_pos(1)?;
                self.ip += 4;
                match pure_opcode {
                    1 => {
                        self.write_mem(save_at, op1 + op2)?;
                    }
                    2 => {
                        self.write_mem(save_at, op1 * op2)?;
                    }
                    _ => {
                        unreachable!();
                    }
                }
            }
            3 => {
                let offset = if self.modes[0] == ParameterMode::Relative {
                    self.relative_base
                } else {
                    0
                };
                self.modes[0] = ParameterMode::Immediate;
                let save_at = (offset + self.get_parameter_for_pos(0)?) as usize;
                if let Some(input_value) = self.input_buf.pop_front() {
                    self.write_mem(save_at, input_value)?;
                    self.ip += 2;
                } else {
                    return Ok(IntCodeInterpreterStatus::AwaitingInput);
                }
            }
            4 => {
                let op = self.get_parameter_for_pos(0)?;
                self.output_buf.push_back(op);
                self.ip += 2;
                return Ok(IntCodeInterpreterStatus::EmittedOutput);
            }
            5 | 6 => {
                let op1 = self.get_parameter_for_pos(0)?;
                let op2 = self.get_parameter_for_pos(1)?;
                if (pure_opcode == 5 && op1 != 0) || (pure_opcode == 6 && op1 == 0) {
                    self.ip = op2 as usize;
                } else {
                    self.ip += 3;
                }
            }
            7 | 8 => {
                let offset = if self.modes[2] == ParameterMode::Relative {
                    self.relative_base
                } else {
                    0
                };
                self.modes[2] = ParameterMode::Immediate;
                let op1 = self.get_parameter_for_pos(0)?;
                let op2 = self.get_parameter_for_pos(1)?;
                let save_at = (offset + self.get_parameter_for_pos(2)?) as usize;
                if (pure_opcode == 7 && op1 < op2) || (pure_opcode == 8 && op1 == op2) {
                    self.write_mem(save_at, 1)?;
                } else {
                    self.write_mem(save_at, 0)?;
                }
                self.ip += 4;
            }
            9 => {
                let op = self.get_parameter_for_pos(0)?;
                self.relative_base += op;
                self.ip += 2;
            }
            99 => {
                self.finished = true;
            }
            _ => {
                fail = true;
            }
        }

        if fail {
            Err(IntCodeInterpreterError {
                msg: format!(
                    "Invalid opcode found at {}: {}",
                    self.ip, self.mem_space[self.ip]
                ),
            })
        } else {
            self.clear_modes();
            Ok(IntCodeInterpreterStatus::Done)
        }
    }

    pub fn get_inputs_for_output(
        program: Vec<i64>,
        output: i64,
    ) -> Result<(i64, i64), IntCodeInterpreterError> {
        // TODO this function will loop if the IntCode program loops
        // TODO so far, IntCode programs will always terminate, perhaps in failure
        // TODO but have no loops for now
        //
        // Some solutions I've seen hardcode the horizons. This may search a bit more but it's
        // failsafe.
        let mut horizon = 0;
        'outer: loop {
            for i1 in 0..=horizon {
                let mut interpreter = IntCodeInterpreter::new(program.clone());
                let i2 = horizon - i1;
                interpreter.write_mem(1, i1)?;
                interpreter.write_mem(2, i2)?;
                if interpreter.run().is_ok() && interpreter.read_mem(0)? == output {
                    break 'outer Ok((i1, i2));
                }
            }

            horizon += 1;
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn step_by_step_test() {
        let program: Vec<i64> = vec![1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50];
        let mut interpreter = super::IntCodeInterpreter::new(program);

        assert_eq!(
            vec![1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50],
            interpreter.mem_space
        );

        interpreter.step().expect("Error in step 1");
        assert_eq!(
            vec![1, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50],
            interpreter.mem_space
        );

        interpreter.step().expect("Error in step 2");
        assert_eq!(
            vec![3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50],
            interpreter.mem_space
        );

        assert_eq!(
            99,
            interpreter
                .read_mem(interpreter.ip)
                .expect("Error reading from memspace")
        );
    }

    #[test]
    fn test_program_1() {
        let program: Vec<i64> = vec![1, 0, 0, 0, 99];
        let mut interpreter = super::IntCodeInterpreter::new(program);

        interpreter.run().expect("Error running program");
        assert_eq!(vec![2, 0, 0, 0, 99], interpreter.mem_space);
    }

    #[test]
    fn test_program_2() {
        let program: Vec<i64> = vec![2, 3, 0, 3, 99];
        let mut interpreter = super::IntCodeInterpreter::new(program);

        interpreter.run().expect("Error running program");
        assert_eq!(vec![2, 3, 0, 6, 99], interpreter.mem_space);
    }

    #[test]
    fn test_program_3() {
        let program: Vec<i64> = vec![2, 4, 4, 5, 99, 0];
        let mut interpreter = super::IntCodeInterpreter::new(program);

        interpreter.run().expect("Error running program");
        assert_eq!(vec![2, 4, 4, 5, 99, 9801], interpreter.mem_space);
    }

    #[test]
    fn test_program_4() {
        let program: Vec<i64> = vec![1, 1, 1, 4, 99, 5, 6, 0, 99];
        let mut interpreter = super::IntCodeInterpreter::new(program);

        interpreter.run().expect("Error running program");
        assert_eq!(vec![30, 1, 1, 4, 2, 5, 6, 0, 99], interpreter.mem_space);
    }

    #[test]
    fn test_program_5() {
        let program: Vec<i64> = vec![1, 0, 0, 4, 99, 5, 6, 0, 99];
        let mut interpreter = super::IntCodeInterpreter::new(program);

        interpreter.run().expect("Error running program");
        assert_eq!(30, interpreter.mem_space[0]);
    }

    #[test]
    fn test_get_input_0() {
        let program: Vec<i64> = vec![1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50];
        assert_eq!(
            (9, 10),
            super::IntCodeInterpreter::get_inputs_for_output(program, 3500).expect("Error")
        );
    }

    #[test]
    fn test_move_input_to_output() {
        let program: Vec<i64> = vec![3, 0, 4, 0, 99];
        let mut interpreter = super::IntCodeInterpreter::new(program);

        interpreter.set_input(42);
        interpreter.run().expect("Error running program");
        assert_eq!(42, interpreter.get_output().unwrap());
    }

    #[test]
    fn test_eq_eight() {
        let program: Vec<i64> = vec![3, 9, 8, 9, 10, 9, 4, 9, 99, -1, 8];

        let mut interpreter = super::IntCodeInterpreter::new(program.clone());
        interpreter.set_input(8);
        interpreter.run().expect("Error running program");
        assert_eq!(1, interpreter.get_output().unwrap());

        interpreter = super::IntCodeInterpreter::new(program.clone());
        interpreter.set_input(7);
        interpreter.run().expect("Error running program");
        assert_eq!(0, interpreter.get_output().unwrap());
    }

    #[test]
    fn test_lt_eight() {
        let program: Vec<i64> = vec![3, 9, 7, 9, 10, 9, 4, 9, 99, -1, 8];

        let mut interpreter = super::IntCodeInterpreter::new(program.clone());
        interpreter.set_input(8);
        interpreter.run().expect("Error running program");
        assert_eq!(0, interpreter.get_output().unwrap());

        interpreter = super::IntCodeInterpreter::new(program.clone());
        interpreter.set_input(7);
        interpreter.run().expect("Error running program");
        assert_eq!(1, interpreter.get_output().unwrap());
    }

    #[test]
    fn test_eq_eight_immediate() {
        let program: Vec<i64> = vec![3, 3, 1108, -1, 8, 3, 4, 3, 99];

        let mut interpreter = super::IntCodeInterpreter::new(program.clone());
        interpreter.set_input(8);
        interpreter.run().expect("Error running program");
        assert_eq!(1, interpreter.get_output().unwrap());

        interpreter = super::IntCodeInterpreter::new(program.clone());
        interpreter.set_input(7);
        interpreter.run().expect("Error running program");
        assert_eq!(0, interpreter.get_output().unwrap());
    }

    #[test]
    fn test_lt_eight_immediate() {
        let program: Vec<i64> = vec![3, 3, 1107, -1, 8, 3, 4, 3, 99];

        let mut interpreter = super::IntCodeInterpreter::new(program.clone());
        interpreter.set_input(8);
        interpreter.run().expect("Error running program");
        assert_eq!(0, interpreter.get_output().unwrap());

        interpreter = super::IntCodeInterpreter::new(program.clone());
        interpreter.set_input(7);
        interpreter.run().expect("Error running program");
        assert_eq!(1, interpreter.get_output().unwrap());
    }

    #[test]
    fn test_eq_zero() {
        let program: Vec<i64> = vec![3, 12, 6, 12, 15, 1, 13, 14, 13, 4, 13, 99, -1, 0, 1, 9];

        let mut interpreter = super::IntCodeInterpreter::new(program.clone());
        interpreter.set_input(1);
        interpreter.run().expect("Error running program");
        assert_eq!(1, interpreter.get_output().unwrap());

        interpreter = super::IntCodeInterpreter::new(program.clone());
        interpreter.set_input(0);
        interpreter.run().expect("Error running program");
        assert_eq!(0, interpreter.get_output().unwrap());
    }

    #[test]
    fn test_eq_zero_immediate() {
        let program: Vec<i64> = vec![3, 3, 1105, -1, 9, 1101, 0, 0, 12, 4, 12, 99, 1];

        let mut interpreter = super::IntCodeInterpreter::new(program.clone());
        interpreter.set_input(1);
        interpreter.run().expect("Error running program");
        assert_eq!(1, interpreter.get_output().unwrap());

        interpreter = super::IntCodeInterpreter::new(program.clone());
        interpreter.set_input(0);
        interpreter.run().expect("Error running program");
        assert_eq!(0, interpreter.get_output().unwrap());
    }

    #[test]
    fn test_cmp_eight() {
        let program: Vec<i64> = vec![
            3, 21, 1008, 21, 8, 20, 1005, 20, 22, 107, 8, 21, 20, 1006, 20, 31, 1106, 0, 36, 98, 0,
            0, 1002, 21, 125, 20, 4, 20, 1105, 1, 46, 104, 999, 1105, 1, 46, 1101, 1000, 1, 20, 4,
            20, 1105, 1, 46, 98, 99,
        ];

        let mut interpreter = super::IntCodeInterpreter::new(program.clone());
        interpreter.set_input(7);
        interpreter.run().expect("Error running program");
        assert_eq!(999, interpreter.get_output().unwrap());

        interpreter = super::IntCodeInterpreter::new(program.clone());
        interpreter.set_input(8);
        interpreter.run().expect("Error running program");
        assert_eq!(1000, interpreter.get_output().unwrap());

        interpreter = super::IntCodeInterpreter::new(program.clone());
        interpreter.set_input(9);
        interpreter.run().expect("Error running program");
        assert_eq!(1001, interpreter.get_output().unwrap());
    }
}
